<?php
namespace App\finalproject\hobbies;
use PDO;
class hobbies
{
    public $id = '';
    public $userid = '';
    public $title = '';
    public $description = '';
    public $image = '';
    public $bio = '';

    public function __construct()
    {
        session_start();
        $this->pdo=new PDO('mysql: host=localhost; dbname=cvbank','root','');
    }

    public function setHobbies($data = '')
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->userid = $data['user_id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        return $this;

    }
    public function hobbiesStore(){
        try {
            $query = "INSERT INTO `hobbies` (`id`, `user_id`, `title`, `description`) VALUES (:id, :user_id, :title,:description);";
            $stmt = $this->pdo->prepare($query);
            $out = $stmt->execute(
                array(
                    ":id" => null,
                    ":user_id" => $this->userid,
                    ":title" => $this->title,
                    ":description" => $this->description
                )
            );
            if ($out) {
                $_SESSION['message'] = "About information update Successfully  !";
                header('location:hobbies.php');
            }
        }catch (PDOException $e){
            echo "ERROR:".$e->getMessage();
        }
    }
    public function hobbies(){
        try{
            $query="SELECT * FROM `hobbies` WHERE user_id="."'".$this->userid."'";
            $stmt=$this->pdo->prepare($query);
            $stmt->execute();
            $data= $stmt->fetchAll();
            return $data;
        }catch (PDOException $e){
            echo "ERROR:".$e->getMessage();
        }
    }
}
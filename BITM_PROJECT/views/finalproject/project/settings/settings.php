<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Setting Page</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="assets/js/core/app.js"></script>
    <script type="text/javascript" src="assets/js/pages/login.js"></script>
    <!-- /theme JS files -->

</head>

<body class="bg-slate-800">

<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Advanced login -->
                <form action="setting_store.php" method="post">
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                           <h4 class="content-group-lg"><i class="icon-cog3  spinner"> </i> Profile Settings <small class="display-block">Enter your credentials</small></h4>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control"name="title" placeholder="Enter Your Title">
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" name="fullname" placeholder="Enter your Full Name">
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <textarea  class="form-control" name="description" placeholder="Enter Description"></textarea>
                        </div>
                        <div class="form-group has-feedback has-feedback-left">
                            <textarea  class="form-control" name="address" placeholder="Enter your Address"></textarea>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="file" name="fileToUpload" id="fileToUpload">
                            <input type="submit" value="Upload Image" name="submit">
                        </div>

                         <div class="form-group">
                            <button type="submit" class="btn bg-blue btn-block" name="submit">Submit <i class="icon-circle-right2 position-right"></i></button>
                        </div>
                        <input type="text" name="user_id" value="<?php echo  $_SESSION['user_info']['unique_id'];?>">
                     </div>
                </form>
                <!-- /advanced login -->

                <!-- Footer -->
                <div class="footer text-white">
                    &copy; 2017. <a href="#">WEB APPS DEVELOPERS</a> by <a href="#" target="_blank">CODE BREAKERS</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>